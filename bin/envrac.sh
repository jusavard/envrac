#!/usr/bin/bash
echo $0
BIN_DIR=$(dirname $(readlink -f ./bin/envrac))
CONF_DIR=$(cd $BIN_DIR/../etc;pwd)
LOG_DIR=$(cd $BIN_DIR/../log;pwd)
. $CONF_DIR/envrac.conf

debug_output_vars_and_exit() 
{
echo RELATIVE_ROOT $RELATIVE_ROOT
echo PACKAGE_LIST_FILE $PACKAGE_LIST_FILE
echo BUILD_LOG_FILE_NAME $BUILD_LOG_FILE_NAME
echo STATUS_FILE_NAME $STATUS_FILE_NAME
echo REPORT_FILE_NAME $REPORT_FILE_NAME
exit 0
}

usage()
{
	echo TODO
	# TODO
	# This usage
	# File presence validation
	# Housekeeping function to flush logs/status files
	# package.list validation
	# mode --preserve --> Do not flush status/report files
	# mode --all that does not use packages.list but create a complete pkgsrc build by create a list from a find in $PREFIX/
	# mode --clean that run a "make clean depends" before running a "make package"
}
build_package_from_list() 
{
for PACKAGE in $PACKAGE_LIST
do
        echo "$PACKAGE BUILD STARTED on $(date +%Y-%m-%d/%H:%M:%S)" >> $STATUS_FILE         
        cd $PREFIX/$PACKAGE
        bmake package >> $BUILD_LOG_FILE_NAME 2>&1
        # I assume if the last line of the build log is "Creating binary package" then the build was a succes
        if grep "Creating binary package" $BUILD_LOG_FILE_NAME
        then
                echo "$PACKAGE BUILD SUCESSFULLY on $(date +%Y-%m-%d/%H:%M:%S)" >> $STATUS_FILE         
        else
                echo "$PACKAGE BUILD FAILED on $(date +%Y-%m-%d/%H:%M:%S)" >> $STATUS_FILE
        fi
done
}
debug_output_vars_and_exit

build_package_from_list
